$(function () {
    var nb = $('#navbtn');
    var n = $('#topnav nav');

    $(window).on('resize', function () {

        if ($(this).width() < 570 && n.hasClass('keep-nav-closed')) {
            // if the nav menu and nav button are both visible,
            // then the responsive nav transitioned from open to non-responsive, then back again.
            // re-hide the nav menu and remove the hidden class
            $('#topnav nav').hide().removeAttr('class');
        }
        if (nb.is(':hidden') && n.is(':hidden') && $(window).width() > 569) {
            // if the navigation menu and nav button are both hidden,
            // then the responsive nav is closed and the window resized larger than 560px.
            // just display the nav menu which will auto-hide at <560px width.
            $('#topnav nav').show().addClass('keep-nav-closed');
        }
    });

    $('#topnav nav a,#topnav h1 a,#btmnav nav a').on('click', function (e) {
        e.preventDefault(); // stop all hash(#) anchor links from loading
    });

    $('#navbtn').on('click', function (e) {
        e.preventDefault();
        $("#topnav nav").slideToggle(350);
    });

    var el = $('input[type=text], textarea');
    el.focus(function (e) {
        if (e.target.value == e.target.defaultValue)
            e.target.value = '';
    });
    el.blur(function (e) {
        if (e.target.value == '')
            e.target.value = e.target.defaultValue;
    });

    var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear'
    };

    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1200);
    });

    $().UItoTop({easingType: 'easeOutQuart'});


    $('#contactForm').submit(function (ev) {

        ev.preventDefault();
        var form = this;
        var required_fields = ['formName', 'formEmail', 'formPhone', 'formSubject'];
        var i;

        for (i in required_fields) {
            if (form[i].value == "" || form[i].value == form[i].defaultValue) {
                sweetAlert(i18n.required + ' ' + form[i].defaultValue);
                return false;
            }
        }

        $.ajax({
            type: "POST",
            url: "https://mandrillapp.com/api/1.0/messages/send.json",
            data: {
                'key': '8k0TCNKm9GxZWaWi4etlUA',
                'message': {
                    'from_email': form.formEmail.value,
                    'to': [
                        {
                            'email': 'kirill@shefer-law.com',
                            'name': 'Kirill Shefer',
                            'type': 'to'
                        }
                    ],
                    'autotext': 'true',
                    'subject': 'Mail from shefer-law.com ' + form.formSubject.value,
                    'html': "<h3>Mail from site</h3>" +
                    "<p><b>NAME</b>: " + form.formName.value + "</p>" +
                    "<p><b>EMAIL</b>: " + form.formEmail.value + "</p>" +
                    "<p><b>PHONE</b>: " + form.formPhone.value + "</p>" +
                    "<p><b>MESSAGE</b>:<br>" + form.formMessage.value + "</p>"
                }
            }
        }).done(function (response) {
            if (response[0].reject_reason === null) {
                $('#contactForm').prepend('<p class="ok">' + i18n.form_ok + '</p>');
            } else {
                sweetAlert(i18n.form_error);
            }
        });
    });

});
